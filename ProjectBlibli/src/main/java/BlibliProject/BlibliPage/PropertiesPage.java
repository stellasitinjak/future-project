package BlibliProject.BlibliPage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesPage {
    Properties prop = new Properties();

    public String getProperty(String propName){
        try (InputStream input = new FileInputStream(System.getProperty("user.dir") + "/config.properties/")) {
            prop.load(input);
        } catch (IOException ex) { ex.printStackTrace(); }
        return prop.getProperty(propName);
    }
}
