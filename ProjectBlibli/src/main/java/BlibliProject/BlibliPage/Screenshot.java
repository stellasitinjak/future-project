package BlibliProject.BlibliPage;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class Screenshot {
    static WebDriver webDriver;

    public Screenshot(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public static String takeSnapShot(String fileName){
        TakesScreenshot screenshot = ((TakesScreenshot)webDriver);
        File src = screenshot.getScreenshotAs(OutputType.FILE);
        File dest = new File((System.getProperty("user.dir") + "/target/screenshot/" + fileName + "-" + System.currentTimeMillis() + ".png"));
        try{
            FileUtils.copyFile(src, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String path = dest.getAbsolutePath();
        return path;
    }
}
