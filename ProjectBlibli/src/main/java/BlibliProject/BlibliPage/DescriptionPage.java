package BlibliProject.BlibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DescriptionPage {
    WebDriver webDriver;

    By xpathAddToBag = By.xpath("//div[@class='add-to-cart__button']//div[@class='button']");
    By xpathBag = By.xpath("//a[@class='b-flex b-ai-center']");
    By xpathBtnBuy = By.xpath("//div[contains(text(),'BELI SEKARANG')]");
    By xpathBtnCheckout = By.xpath("//button[@class='blu-btn b-primary']");
    By xpathQuantity = By.xpath("//div[@class='quantity__increase']");
    By xpathMerchant = By. xpath("//div[@class='merchant__url']");

    public DescriptionPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void visitMerchant() throws InterruptedException {
        WebElement merchant = webDriver.findElement(xpathMerchant);
        merchant.click();
        Thread.sleep(5000);
    }

    public void addQuantity() throws InterruptedException {
        WebElement quantity = webDriver.findElement(xpathQuantity);
        quantity.click();
        Thread.sleep(5000);
    }

    public void btnAddToBag() throws InterruptedException {
        WebElement btnAddToBag = webDriver.findElement(xpathAddToBag);
        btnAddToBag.click();
        Thread.sleep(5000);
    }

    public void btnOpenBag() throws InterruptedException {
        WebElement btnOpenBag = webDriver.findElement(xpathBag);
        btnOpenBag.click();
        Thread.sleep(5000);
    }

    public void btnBuyNow() throws InterruptedException {
        WebElement btnBuy = webDriver.findElement(xpathBtnBuy);
        btnBuy.click();
        Thread.sleep(10000);
    }

    public void btnCheckout() throws InterruptedException {
        Thread.sleep(10000);
        WebElement btnCheckout = webDriver.findElement(xpathBtnCheckout);
        btnCheckout.click();
        Thread.sleep(5000);
    }
}
