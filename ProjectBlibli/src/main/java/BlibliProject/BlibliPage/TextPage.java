package BlibliProject.BlibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TextPage {
    WebDriver webDriver;
    String find = null;

    By xpathTxtFavorit = By.xpath("//h4[@class='favourite__title']");
    By xpathTxtGulaku = By.xpath("//span[contains(text(),'\"gulaku\"')]");
    By xpathBtnBeli = By.xpath("//div[contains(text(),'BELI SEKARANG')]");
    By xpathLogin = By.xpath("//h2[@class='title']");
    By xpathSearch = By.xpath("//h3[contains(text(),'Beritahu kami apa yang sedang Anda cari')]");
    By xpathAkun = By.xpath("//div[@class='account__text']");
    By xpathWrAkun = By.xpath("//div[@class='error']");
    By xpathgetBtnMasuk = By.xpath("//button[@class='blu-btn btn__login b-outline b-small']//div[@class='blu-ripple'][contains(text(),'Masuk')]");
    By xpathBag = By.xpath("//div[@class='cart__amt']");
    By xpathgetCartTitle = By.xpath("//span[@class='cart__title__bag__text']");
    By xpathBtngetBtnCheckout = By.xpath("//button[@class='blu-btn b-primary']");
    By xpathBlibliMart = By.xpath("//span[contains(text(),'BlibliMart')]");
    By xpathQuantity = By.xpath("//div[@class='quantity__field']//input");
    By xpathMerchant = By.xpath("//span[@class='rating__score--title-text']");
    By xpathPesanan = By.xpath("//div[@class='page-title']//span[@class='ng-binding'][contains(text(),'Pesanan')]");
    By xpathPengembalian = By.xpath("//div[@class='page-title']");

    public TextPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public String getTxtPengembalian() {
        try{ find = webDriver.findElement(xpathPengembalian).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getTxtPesanan() {
        try{ find = webDriver.findElement(xpathPesanan).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getTxtMerchant() {
        try{ find = webDriver.findElement(xpathMerchant).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getQuantity() {
        try{ find = webDriver.findElement(xpathQuantity).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getTxtBlibliMart() {
        try{ find = webDriver.findElement(xpathBlibliMart).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getTxtHomepage() {
        try{ find = webDriver.findElement(xpathTxtFavorit).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getTxtGulaku() {
        try{ find = webDriver.findElement(xpathTxtGulaku).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getSearch() {
        try{ find = webDriver.findElement(xpathSearch).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getBtnBeli() {
        try{ find = webDriver.findElement(xpathBtnBeli).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getTxtLogin() {
        try{ find = webDriver.findElement(xpathLogin).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getAkun() {
        try{ find = webDriver.findElement(xpathAkun).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getWrongAkun() {
        try{ find = webDriver.findElement(xpathWrAkun).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getBtnMasuk() {
        try{ find = webDriver.findElement(xpathgetBtnMasuk).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getBagAmount() {
        try{ find = webDriver.findElement(xpathBag).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getCartTitle() {
        try{ find = webDriver.findElement(xpathgetCartTitle).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }

    public String getBtnCheckout() {
        try{ find = webDriver.findElement(xpathBtngetBtnCheckout).getText();
        } catch (Exception e) { e.printStackTrace(); }
        return find;
    }


}
