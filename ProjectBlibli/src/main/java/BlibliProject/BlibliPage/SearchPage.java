package BlibliProject.BlibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {
    WebDriver webDriver;

    By xpathSearch = By.xpath("//input[@placeholder='Kamu lagi cari apa?']");
    By xpathBtnSearch = By.xpath("//button[@class='blu-btn b-secondary b-small']");
    By xpathBarang = By.xpath("//body/div[@class='background-grey']/div[@class='collabs-wrapper']/div/div/div[@class='container']/div[@id='blibli-main-ctrl']/section[@class='content-section']/div[@id='catalogProductListDiv']/div[@class='product-listing']/div[@id='productListingAgeRestrictedFilter']/div[@class='large-16 medium-16 small-16']/div[@id='catalogProductListContentDiv']/div[@class='product columns']/div[1]/div[1]/div[1]/a[1]/div[1]/div[2]/div[1]");
    By xpathKategori = By.xpath("//div[@class='kategori__trigger b-flex b-ai-center']");
    By xpathBlibliMart = By.xpath("//a[contains(text(),'BlibliMart')]");

    public SearchPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void openKategori() throws InterruptedException {
        WebElement kategori = webDriver.findElement(xpathKategori);
        kategori.click();
        Thread.sleep(5000);
        WebElement blibliMart = webDriver.findElement(xpathBlibliMart);
        blibliMart.click();
        Thread.sleep(5000);
    }

    public void search(String barang) throws InterruptedException {
        WebElement searchBar = webDriver.findElement(xpathSearch);
        searchBar.sendKeys(barang);
//        searchBar.sendKeys(Keys.ENTER);
        Thread.sleep(5000);
        WebElement btnSearch = webDriver.findElement(xpathBtnSearch);
        btnSearch.click();
        Thread.sleep(20000);
    }

    public void clickSomethingOnList() throws InterruptedException {
        WebElement barang = webDriver.findElement(xpathBarang);
        barang.click();
        Thread.sleep(10000);
    }
}
