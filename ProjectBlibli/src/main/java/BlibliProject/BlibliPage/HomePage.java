package BlibliProject.BlibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class HomePage {
    WebDriver webDriver;

    By xpathLogoBlibli = By.xpath("//a[@class='router-link-active']//img");
    By xpathLogin = By.xpath("//button[@class='blu-btn btn__login b-outline b-small']");
    By xpathLogoUser  = By.xpath("//div[@class='account tooltip__trigger']//div[@class='b-flex b-ai-center']");
    By xpathLogout = By.xpath("//span[contains(text(),'Keluar')]");
    By xpathPesanan = By.xpath("//a[contains(text(),'Cek Status Pesanan')]");
    By xpathPengembalian = By.xpath("//div[@class='large-3 medium-3 small-3 columns']//li[3]//a[1]");

    public HomePage(WebDriver webDriver) { this.webDriver = webDriver; }

    public void openHomePage(){ webDriver.get("https://www.blibli.com/"); }

    public void cekPengembalian() throws InterruptedException {
        WebElement cekPengembalian = webDriver.findElement(xpathPengembalian);
        cekPengembalian.click();
        Thread.sleep(5000);
    }

    public void cekPesanan() throws InterruptedException {
        WebElement cekPesanan = webDriver.findElement(xpathPesanan);
        cekPesanan.click();
        Thread.sleep(5000);
    }

    public void Login() throws InterruptedException {
        WebElement btnLogin = webDriver.findElement(xpathLogin);
        btnLogin.click();
        Thread.sleep(10000);
    }

    public void Logout() throws InterruptedException {
        Thread.sleep(1500);
        WebElement logoBlibli = webDriver.findElement(xpathLogoBlibli);
        logoBlibli.click();
        Thread.sleep(20000);
        try { new Actions(webDriver).sendKeys(Keys.ESCAPE).perform(); }
        catch (Exception e){ System.out.println(e.getMessage()); }
        WebElement btnAkun = webDriver.findElement(xpathLogoUser);
        btnAkun.click();
        Thread.sleep(1500);
        WebElement btnLogout = webDriver.findElement(xpathLogout);
        btnLogout.click();
        Thread.sleep(5000);
    }

    public void escape() {
        try { new Actions(webDriver).sendKeys(Keys.ESCAPE).perform(); }
        catch (Exception e){ System.out.println(e.getMessage()); }
    }
}
