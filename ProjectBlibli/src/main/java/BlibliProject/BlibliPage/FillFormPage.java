package BlibliProject.BlibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FillFormPage {
    WebDriver webDriver;

    By xpathEmail = By.xpath("//input[@placeholder='Masukkan email']");
    By xpathPassword = By.xpath("//input[@placeholder='Masukkan kata sandi']");

    public FillFormPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void fillForm(String email, String password) throws InterruptedException {
        WebElement formEmail = webDriver.findElement(xpathEmail);
        formEmail.sendKeys(email);
        Thread.sleep(1500);
        WebElement formPassword = webDriver.findElement(xpathPassword);
        formPassword.sendKeys(password);
        Thread.sleep(1500);
        formPassword.sendKeys(Keys.ENTER);
        Thread.sleep(60000);
    }
}
