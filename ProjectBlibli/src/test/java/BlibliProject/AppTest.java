package BlibliProject;

import BlibliProject.BlibliPage.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
public class AppTest {
    WebDriver webDriver;
    PropertiesPage propPage = new PropertiesPage();

    String browser;
    String email;
    String pass;
    String barang;
    static ExtentTest test;
    static ExtentReports report;

    /**
     * Rigorous Test :-)
     */
    public void report(String actual, String expected, String phase, String fileName) {
        Screenshot shot = new Screenshot(webDriver);
        if (actual.equals(expected)) {
            test.log(LogStatus.PASS, phase, "'" + expected + "' ditemukan" + test.addScreenCapture(shot.takeSnapShot(fileName)));
        } else {
            test.log(LogStatus.FAIL, phase, "'" + expected  + "' tidak ditemukan" + test.addScreenCapture(shot.takeSnapShot(fileName)));
        }
    }

    @Before
    public void initiate(){
        report = new ExtentReports(System.getProperty("user.dir") + "\\ExtentReportResults.html");
        browser = propPage.getProperty("prop.Browser");
        if(browser.equalsIgnoreCase("Chrome")){
            WebDriverManager.chromedriver().setup();
            webDriver = new ChromeDriver();
        }
        else{
            WebDriverManager.firefoxdriver().setup();
            webDriver = new FirefoxDriver();
        }
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void done() {
        report.endTest(test);
        report.flush();
        if (webDriver != null) {
            webDriver.quit();
        }
    }

    @Test
    public void sequentialTest() throws Exception {
        loginNoAccount();
        login();
        blibliMart();
        addQuantity();
        visitMerchant();
        searchFound();
        searchNotFound();
        addToBagLogin();
        addToBagNotLogin();
        retailTestLogin();
        retailTestNotLogin();
        openBagLogin();
        openBagNotLogin();
        cekStatusPesanan();
        cekPengembalian();
        checkout();
    }

    @Test
    public void login() throws InterruptedException {
        FillFormPage fillForm = new FillFormPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Login");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageLogin");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "Login");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillForm");

        homePage.Logout();
        report(text.getBtnMasuk(), "Masuk", "Logout", "Logout");
    }

    @Test
    public void loginNoAccount() throws InterruptedException {
        FillFormPage fillForm = new FillFormPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Wrong.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Login.No.Account");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageLNA");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "Login");
        fillForm.fillForm(email, pass);
        report(text.getWrongAkun(), "Maaf, email atau kata sandi yang kamu masukkan salah.", "Fill form", "fillFormLNA");
    }

    @Test
    public void searchFound() throws InterruptedException {
        SearchPage searchPage = new SearchPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        barang = propPage.getProperty("prop.Nama.Barang");
        test = report.startTest("Search.Found");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageSF");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search" + barang,  "SearchSF");
    }

    @Test
    public void searchNotFound() throws InterruptedException {
        SearchPage searchPage = new SearchPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        barang = propPage.getProperty("prop.Barang.Not.Found");
        test = report.startTest("Search.Not.Found");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageSNF");
        searchPage.search(barang);
        report(text.getSearch(), "Beritahu kami apa yang sedang Anda cari", "Search Barang", "SearchNF");
    }

    @Test
    public void checkout() throws InterruptedException {
        FillFormPage fillForm = new FillFormPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Checkout.Login");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageCL");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "LoginCL");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillFormCL");
        descriptionPage.btnOpenBag();
        report(text.getCartTitle(), "Bag", "Open Bag", "BagCL");
        descriptionPage.btnCheckout();
        report(text.getBtnCheckout(), "Pengiriman", "Checkout", "CheckoutCL");

        homePage.Logout();
        report(text.getBtnMasuk(), "Masuk", "Logout", "Logout");
    }

    @Test
    public void openBagLogin() throws InterruptedException {
        FillFormPage fillForm = new FillFormPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Open.Bag.Login");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageOBL");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "LoginOBL");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillFormOBL");
        descriptionPage.btnOpenBag();
        report(text.getCartTitle(), "Bag", "Open Bag", "BagOBL");

        homePage.Logout();
        report(text.getBtnMasuk(), "Masuk", "Logout", "Logout");
    }

    @Test
    public void openBagNotLogin() throws InterruptedException {
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        test = report.startTest("Open.Bag.Not.Login");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageOBNL");
        descriptionPage.btnOpenBag();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Open Bag", "BagOBNL");
    }

    @Test
    public void retailTestLogin() throws Exception {
        FillFormPage fillForm = new FillFormPage(webDriver);
        SearchPage searchPage = new SearchPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Retail.Test.Login");
        barang = propPage.getProperty("prop.Nama.Barang");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageRL");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "LoginRL");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillFormRL");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search" + barang,  "SearchRL");
        searchPage.clickSomethingOnList();
        report(text.getBtnBeli(), "BELI SEKARANG", "getBtnBeli " + barang,  "getBtnBeliRL");
        descriptionPage.btnBuyNow();
        report(text.getCartTitle(), "Bag", "Buy Now", "BuyRL");

        homePage.Logout();
        report(text.getBtnMasuk(), "Masuk", "Logout", "Logout");
    }

    @Test
    public void retailTestNotLogin() throws InterruptedException, IOException {
        SearchPage searchPage = new SearchPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        barang = propPage.getProperty("prop.Nama.Barang");
        test = report.startTest("Retail.Test.Not.Login");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageRNL");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search" + barang,  "SearchRNL");
        searchPage.clickSomethingOnList();
        report(text.getBtnBeli(), "BELI SEKARANG", "getBtnBeli " + barang,  "getBtnBeliRNL");
        descriptionPage.btnBuyNow();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Buy Now", "BuyRNL");
    }

    @Test
    public void addToBagLogin() throws InterruptedException {
        FillFormPage fillForm = new FillFormPage(webDriver);
        SearchPage searchPage = new SearchPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Add.To.Bag.Login");
        barang = propPage.getProperty("prop.Nama.Barang");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageBL");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "LoginBL");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillFormBL");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search" + barang,  "SearchBL");
        searchPage.clickSomethingOnList();
        report(text.getBtnBeli(), "BELI SEKARANG", "getBtnBeli " + barang,  "getBtnBeliBL");
        descriptionPage.btnAddToBag();
        report(text.getBagAmount(), "1", "Add To Bag " + barang,  "AddBL");

        homePage.Logout();
        report(text.getBtnMasuk(), "Masuk", "Logout", "Logout");
    }

    @Test
    public void addToBagNotLogin() throws InterruptedException {
        SearchPage searchPage = new SearchPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        barang = propPage.getProperty("prop.Nama.Barang");
        test = report.startTest("Add.To.Bag.Not.Login");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageBNL");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search " + barang,  "SearchBNL");
        searchPage.clickSomethingOnList();
        report(text.getBtnBeli(), "BELI SEKARANG", "getBtnBeli " + barang,  "getBtnBeliBNL");
        descriptionPage.btnAddToBag();
        report(text.getBagAmount(), "1", "Add To Bag " + barang,  "AddBNL");
    }

    @Test
    public void blibliMart() throws InterruptedException {
        SearchPage searchPage = new SearchPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        test = report.startTest("Blibli.Mart");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageBM");
        searchPage.openKategori();
        report(text.getTxtBlibliMart(), "BlibliMart", "Open BlibliMart", "blibliMartBM");
    }

    @Test
    public void addQuantity() throws InterruptedException {
        SearchPage searchPage = new SearchPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        barang = propPage.getProperty("prop.Nama.Barang");
        test = report.startTest("Add.Quantity");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageAQ");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search " + barang,  "SearchAQ");
        searchPage.clickSomethingOnList();
        report(text.getBtnBeli(), "BELI SEKARANG", "Pilih Barang " + barang,  "getBtnBeliAQ");
        descriptionPage.addQuantity();
        report(text.getQuantity(), "", "Tambah Kuantitas " + barang,  "QuantityAQ");
    }

    @Test
    public void visitMerchant() throws InterruptedException {
        SearchPage searchPage = new SearchPage(webDriver);
        DescriptionPage descriptionPage = new DescriptionPage(webDriver);
        HomePage homePage = new HomePage(webDriver);
        TextPage text = new TextPage(webDriver);

        barang = propPage.getProperty("prop.Nama.Barang");
        test = report.startTest("Visit.Merchant");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageVM");
        searchPage.search(barang);
        report(text.getTxtGulaku(), "\"gulaku\"", "Search " + barang,  "SearchVM");
        searchPage.clickSomethingOnList();
        report(text.getBtnBeli(), "BELI SEKARANG", "Pilih Barang " + barang,  "getBtnBeliVM");
        descriptionPage.visitMerchant();
        report(text.getTxtMerchant(), "Rating Merchant", "Kunjungi Merchant " + barang,  "MerchantVM");
    }

    @Test
    public void cekStatusPesanan() throws InterruptedException {
        HomePage homePage = new HomePage(webDriver);
        FillFormPage fillForm = new FillFormPage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Cek.Status.Pesanan");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageCSP");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "LoginCSP");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillFormCSP");
        homePage.cekPesanan();
        report(text.getTxtPesanan(), "Pesanan", "Cek Status Pesanan", "PesananCSP");
    }

    @Test
    public void cekPengembalian() throws InterruptedException {
        HomePage homePage = new HomePage(webDriver);
        FillFormPage fillForm = new FillFormPage(webDriver);
        TextPage text = new TextPage(webDriver);

        email = propPage.getProperty("prop.Email");
        pass = propPage.getProperty("prop.Password");
        test = report.startTest("Cek.Pengembalian");

        homePage.openHomePage();
        homePage.escape();
        report(text.getTxtHomepage(), "Favoritnya kamu", "Open Homepage", "HomepageCP");
        homePage.Login();
        report(text.getTxtLogin(), "Masuk ke akun kamu", "Login", "LoginCP");
        fillForm.fillForm(email, pass);
        report(text.getAkun(), "Hi, Stella Ditri", "Fill form", "fillFormCP");
        homePage.cekPesanan();
        report(text.getTxtPesanan(), "Pesanan", "Cek Status Pesanan", "PesananCP");
        homePage.cekPengembalian();
        report(text.getTxtPengembalian(), "Pengembalian", "Cek Status Pengembalian", "PengembalianCP");
    }
}
